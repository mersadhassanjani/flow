FROM continuumio/miniconda3:latest
MAINTAINER Fangyu Wu (fangyuwu@berkeley.edu)

RUN conda install python=3.7
RUN apt-get update && apt-get dist-upgrade -y
RUN apt-get -y upgrade && \
	apt-get install -y \
    vim \
    apt-utils 
RUN apt-get clean all


RUN mkdir stp
COPY /requirements.txt ./stp
COPY /setup.py ./stp
RUN cd stp && sed -i 's/redis~=2.10.6/redis~=3.3.2/g' requirements.txt && pip install -r requirements.txt && cd ..



#FLOW
RUN apt-get install -y make automake gcc g++ subversion python3-dev
RUN ARCHFLAGS=-Wno-error=unused-command-line-argument-hard-error-in-future pip install --upgrade numpy



# SUMO dependencies
RUN apt-get install -y \
	cmake \
	build-essential \
	swig \
	libgdal-dev \
	libxerces-c-dev \
	libproj-dev \
	libfox-1.6-dev \
	libxml2-dev \
	libxslt1-dev 
RUN apt-get install -y sumo sumo-tools sumo-doc
# SUMO


RUN mkdir sumo_binaries 
COPY ./bin ./sumo_binaries


# Startup process
ENV SUMO_HOME=$HOME/sumo/
ENV PATH=$HOME/sumo/bin:$PATH
ENV PYTHONPATH=$PYTHONPATH:$HOME/sumo/tools
